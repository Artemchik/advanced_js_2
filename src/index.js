const parent = document.querySelector("#root");
const list = document.createElement("ul");

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];


parent.appendChild(list);

books.forEach(elem =>{
    try{
      if(!elem.author){
        throw new Error(`${books.indexOf(elem)} - no author here`);
      }else if(!elem.name){
        throw new Error(`${books.indexOf(elem)} - no name here`);
      }else if(!elem.price){
        throw new Error(`${books.indexOf(elem)} - no price here`);
      }else{
        let listItem = document.createElement("li");
        const booksContent = [elem.author, elem.name, elem.price]
        list.appendChild(listItem);
        listItem.innerText = booksContent;
      }
    }
    catch(error){
      console.log(error.message)
    }
})